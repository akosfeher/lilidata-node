export function flatten(object, target, path) {
  path = path || ''
  Object.keys(object).forEach(function(key) {
    if (object[key] && typeof object[key] === 'object') {
      flatten(object[key], target, path + '.' + key)
      return
    }
    target[path + '.' + key] = object[key]
  })
}

export function createArray(obj) {
  const retarr = []
  Object.keys(obj).map(e => {
    const ret = {}
    ret.key = e.substr(1, e.length - 1)
    ret.value = obj[e]
    retarr.push(ret)
  })

  console.log(retarr)
  return retarr
}

export function validateNewkey(edit, newkey, langArr) {
  for (const lan of langArr) {
    if (lan.key === newkey && edit !== true) {
      console.log('***********************************************equals')
      return 'Key exists'
    }
    if (newkey && newkey.startsWith(lan.key + '.')) {
      return 'Key ' + lan.key + ' is exist, can not be a subkey'
    }
    const cs = checkSubkey(lan.key, newkey)
    if (cs) {
      return cs
    }
  }
  return true
}

function checkSubkey(key, newkey) {
  const subkeys = getSubkeys(key)
  for (const subkey of subkeys) {
    if (subkey === newkey) {
      return 'Existing subkey'
    }
  }
  return null
}

function getSubkeys(key) {
  const subkeys = []
  const parts = key.split('.')
  for (const ind in parts) {
    let subkey = parts[0]
    for (let i = 1; i < ind - 1; i++) {
      subkey = subkey + '.' + parts[i]
    }
    subkeys.push(subkey)
  }
  if (key.startsWith('email.tes')) {
    console.log('subkeys', subkeys)
  }
  return subkeys
}
