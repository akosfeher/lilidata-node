import axios from 'axios'
/*
 ** login Rest API call
 */
export default {
  auth: {
    me: () => axios.get('me'),
    login: (apiUrlRuntime, data) => {
      console.log(
        '..... axios before login data.name: ' +
          data.name +
          ' process.env.API_URL: ' +
          process.env.API_URL +
          '   API_URL_RUNTIME ' +
          apiUrlRuntime
      )
      if (!data.ceg) {
        data.ceg = 'admin'
      }
      return axios.post(apiUrlRuntime + 'admin/' + data.ceg + '/login', data)
    }
  },
  some: {
    envs: () => process.env
  }
}
