import cookies from 'js-cookie'
import { setAuthToken, resetAuthToken } from '~/utils/auth'

/*
 ** Set the authorization token from the x-access-token cookie
 */
const token = cookies.get('x-access-token')

if (token) setAuthToken(token)
else resetAuthToken()
