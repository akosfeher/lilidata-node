import cookie from 'cookie'
import axios from 'axios'
import { setAuthToken, resetAuthToken } from '~/utils/auth'

export const state = () => ({
  actMenu: 'orig',
  expired: false,
  unitTypes: [],
  users: [],
  companies: [],
  token: '',
  companyName: '',
})

export const mutations = {
  setExpired(store, expired) {
    console.log('setExpired mutation: ' + expired)
    store.expired = expired
  },
  setActMenu(store, actMenuTitle) {
    console.log('store setActMenu: ' + actMenuTitle)
    store.actMenu = actMenuTitle
  },
  setUnitTypes(store, types) {
    store.unitTypes = types
  },
  updateUser(store, { index, editedUser }) {
    console.log('updateUser ind= ' + index + ' username= ' + editedUser.username)
    Object.assign(store.users[index], editedUser)
  },
  addUser(store, us) {
    console.log('addUser: ' + JSON.stringify(us))
    store.users.push(us)
  },
  setUsers(store, us) {
    console.log('setUsers: ' + us[0].ceg)
    store.users = us
  },
  setCompanies(store, companies) {
    console.log('setCompanies: ' + JSON.stringify(companies[0]))
    store.companies = companies
  },
  setToken(store, token) {
    console.log('setToken: ' + token)
    store.token = token
  },
  setCompanyName(store, name) {
    console.log('setCompanyName: ' + name)
    store.companyName = name
  },
}

export const getters = {
  getUnitTypes(store) {
    return store.unitTypes
  },
  getCompanies(store) {
    return store.companies
  },
  getToken(store) {
    return store.token
  },
  getCompanyName(store) {
    if (store.companyName) return store.companyName
    for (const ind in store.companies) {
      if (store.companies[ind].ceg_id === store.company) {
        // store.companyName = store.companies[ind].ceg_name
        return store.companies[ind].ceg_name
      }
    }
    return store.companyName
  },
  getUsers(store) {
    return store.users
  },
  isUsernameInUse(store) {
    return (username) => {
      // console.log('store   ' + store.users[0].username)
      // console.log(store.users.length)
      for (let ind = 0; ind < store.users.length; ind++) {
        const user = store.users[ind]
        // console.log('checked username= ' + username + '   store username= ' + user.username)
        if (user.active && user.username === username) {
          return true
        }
      }
      return false
    }
  },
  isAuthenticated(store) {
    return store.state && store.state.auth && !!store.state.auth
  },
  isExpired(store) {
    return store.expired
  },
}
export const actions = {
  async nuxtServerInit({ commit, dispatch }, context) {
    const apiUrlLocalRuntime = this.$env.API_URL_LOCAL_RUNTIME
    console.log('nuxtServerInit url: ' + apiUrlLocalRuntime + 'admin')
    const clientUrl = this.$env.CLIENT_URL
    console.log('nuxtServerInit clientUrl: ' + clientUrl)

    /*
    try {
      const url = apiUrlLocalRuntime + 'admin/companies'
      console.log('companies url: ' + url)
      const comps = await this.$axios.get(url)
      // console.log('store countr: ' + JSON.stringify(countr.data))
      // const countries = countr.data()
      commit('setCompanies', comps.data)
    } catch (e) {
      console.log('get companies error ' + e)
    }

    const users = await axios.get(apiUrlLocalRuntime + 'admin/admin')
    console.log('commit= ' + commit)
    console.log('users 1 ' + JSON.stringify(users.data[0]))
    commit('setUsers', users.data)
    */
    return new Promise((resolve, reject) => {
      const cookies = cookie.parse(context.req.headers.cookie || '')
      // eslint-disable-next-line no-prototype-builtins
      if (cookies.hasOwnProperty('x-access-token')) {
        setAuthToken(cookies['x-access-token'])
        dispatch('auth/fetch')
          .then((result) => {
            resolve(true)
          })
          .catch((error) => {
            console.log('fetch user error', error)
            resetAuthToken()
            resolve(false)
          })
      } else {
        resetAuthToken()
        resolve(false)
      }
    })
  },
  async refreshCompanies(context) {
    // const apiUrlLocalRuntime = this.$env.API_URL_LOCAL_RUNTIME
    const apiUrlRuntime = this.$env.API_URL_RUNTIME
    try {
      const url = apiUrlRuntime + 'admin/companies'
      console.log('companies url: ' + url)
      const comps = await this.$axios.get(url)
      // console.log('store countr: ' + JSON.stringify(countr.data))
      // const countries = countr.data()
      this.commit('setCompanies', comps.data)
    } catch (e) {
      console.log('get companies error ' + e)
    }
  },
  getSzervezet(context) {
    const apiUrlLocalRuntime = this.$env.API_URL_LOCAL_RUNTIME
    console.log('getSzervezet url: ' + apiUrlLocalRuntime + process.env.CEG_ID + '/admin/szervezet')
    axios.get(apiUrlLocalRuntime + process.env.CEG_ID + '/admin/szervezet').then((resp) => {
      console.log(resp.data.unitTypes)
      context.commit('setUnitTypes', resp.data.unitTypes)
    })
  },
}
