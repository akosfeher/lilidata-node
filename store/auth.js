import cookies from 'js-cookie'
import api from '~/api'
import { setAuthToken, resetAuthToken } from '~/utils/auth'

export const state = () => ({
  user: null,
})

export const mutations = {
  set_user(store, data) {
    store.user = data
  },
  reset_user(store) {
    store.user = null
  },
}

export const actions = {
  fetch({ commit }) {
    return api.auth
      .me()
      .then((response) => {
        commit('set_user', response.data.result)
        return response
      })
      .catch((error) => {
        commit('reset_user')
        return error
      })
  },
  login({ commit }, data) {
    const apiUrlRuntime = this.$env.API_URL_RUNTIME
    return api.auth.login(apiUrlRuntime, data).then((response) => {
      console.log('user: ' + JSON.stringify(response.data.user) + '  token: ' + response.data.token)
      commit('set_user', response.data.user)
      setAuthToken(response.data.token)
      cookies.set('x-access-token', response.data.token, { expires: 7 })
      return response
    })
  },
  authchecker({ commit }, store) {
    if (store && store.getters.isExpired) return false
    const token = cookies.get('x-access-token')

    if (token == null) {
      return false
    }

    try {
      const data = JSON.parse(decodeURIComponent(escape(window.atob(token.split('.')[1]))))
      const most = Date.now() / 1000
      // console.log('authchecker ' + data.exp + '   Date.now() / 1000 = ' + most)
      if (most > data.exp) {
        return false
      }

      commit('set_user', data)
      setAuthToken(token)
    } catch (e) {
      return false
    }

    return true
  },
  reset({ commit }) {
    commit('reset_user')
    resetAuthToken()
    cookies.remove('x-access-token')
    return Promise.resolve()
  },
}
